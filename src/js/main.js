$(document).ready(function () {

  var burger = $('.burger');
  var menu = $('.menu');
  var menuClose = $('.menu__close-btn');

  $(burger).on('click', function () {
    $('.menu').addClass('menu--is-active');
    disableBodyScroll(menu);
  })

  $(menuClose).on('click', function () {
    $('.menu').removeClass('menu--is-active');
    enableBodyScroll(menu);
  })

  var newItemsSlider = new Swiper('.new-items__slider .swiper-container', {
    slidesPerView: 2,
    spaceBetween: 20,
    draggable: true,
    navigation: {
      prevEl: '.new-items__slider-nav .slider-nav__btn--prev',
      nextEl: '.new-items__slider-nav .slider-nav__btn--next'
    },
    pagination: {
      el: '.new-items__slider-progressbar',
      type: 'progressbar',
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      640: {
        slidesPerView: 2
      },
      992: {
        slidesPerView: 3
      }
    }
  });

  var alsoBuySlider = new Swiper('.also-buy__slider .swiper-container', {
    slidesPerView: 2,
    spaceBetween: 20,
    draggable: true,
    navigation: {
      prevEl: '.also-buy__slider-nav .slider-nav__btn--prev',
      nextEl: '.also-buy__slider-nav .slider-nav__btn--next'
    },
    pagination: {
      el: '.also-buy__slider-progressbar',
      type: 'progressbar',
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      640: {
        slidesPerView: 2
      },
      992: {
        slidesPerView: 3
      }
    }
  });

  var newSpecialOffersSlider = new Swiper('.special-offers__slider .swiper-container', {
    slidesPerView: 2,
    spaceBetween: 20,
    draggable: true,
    navigation: {
      prevEl: '.special-offers__slider-nav .slider-nav__btn--prev',
      nextEl: '.special-offers__slider-nav .slider-nav__btn--next'
    },
    pagination: {
      el: '.special-offers__slider-progressbar',
      type: 'progressbar',
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      640: {
        slidesPerView: 2
      },
      992: {
        slidesPerView: 3
      }
    }
  });

  var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 10,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    direction: 'vertical',
    allowTouchMove: false,
    allowSlideNext: false,
    allowSlidePrev: false
  });

  var galleryTop = new Swiper('.gallery-top', {
    slidesPerView: 1,
    pagination: {
      el: '.product__slider-progressbar',
      type: 'progressbar'
    },
    thumbs: {
      swiper: galleryThumbs
    }
  });

});